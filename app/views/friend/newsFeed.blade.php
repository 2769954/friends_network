@extends ('master')

@section('title')
    Friend's NewsFeed
@stop

@section('content')
    <h5 class= "newsfeed"> 
            @if (Auth::check())
                {{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}
            @endif
    </h5> 
    <div class='row'>
          
            <div class="first-heading">
              <h2 id="main-heading">{{{$friend->fullname}}}'s NewsFeed</h2>
            </div>
          
            <!--<div class='col-sm-12'>-->
                 
            @if (Auth::check())
               
                <div class="col-sm-3">
                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <span class="visible-xs navbar-brand">Sidebar menu</span>
                                </div>
                                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                  <ul class="nav navbar-nav">
                                    <!--<li class="active">{{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}</li>-->
                                    <li>{{ link_to_route('user.newsFeed',"My News Feed",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('friend.show',"My Friends List",array(Auth::user() -> id ) ) }}</li>
                                     
                                    <!--<li><a href="#">Reviews <span class="badge">1,118</span></a></li>-->
                                  </ul>
                                </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
                 <div class='col-sm-6'>
                 <!--<h3 class='well'>Public Posts</h3>-->
                        @if($posts) 
                            @foreach($posts as $post)
                             
                               <div class="panel">
                                      <div class="panel-heading">
                                            {{{$post->author}}}
                                            <div class ="text-center"> Title: {{$post->title}}
                                                @if($post->user_id == Auth::user()->id) 
                                                    <div class="dropdown">
                                                      <button type="button" data-toggle="dropdown"><span class="caret"></span></button>
                                                      <ul class="dropdown-menu">
                                                          <li>{{ link_to_route('post.edit','Edit',array($post->id)) }}</li>
                                                       </ul>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class=post-body>
                                                Message: {{{ $post->message}}}
                                            </div>
                                         </div>
                                            <div class="panel-footer">
                                                 <ul class="list-inline">
                                                       <li>{{ link_to_route('comment.show',"Comments",array($post->id)) }} </li>
                                                       @if($post->user_id == Auth::user()->id)    
                                                            <div class = "delete">
                                                               {{ Form :: open(array('method' => 'DELETE' , 'route' => array('post.destroy',$post->id))) }}
                                                               {{ Form :: submit('Delete',array('class' => 'btn btn-danger'))  }}
                                                               {{Form :: close() }}
                                                             </div>
                                                        @endif  
                                                  </ul>
                                            </div>
                                    </div>
                    @endforeach
                @endif
                 </div>  
                <div class="col-sm-3 newsfeed">
                    {{ link_to_route('friend.checkFriendship', "Friend's profile page",array($friend->id,Auth::user() -> id))}}
                </div>
                
            @else
               <p> You need to login !!</p> 
            @endif 
                
            

           
      </div>
  
@stop

