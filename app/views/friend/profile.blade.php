@extends ('master')

@section('title')
    Profile
@stop

@section('content')
   
    <div class='row'>
          <h5 class= "newsfeed"> 
            @if (Auth::check())
                {{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}
            @endif
            </h5>   
        @if($friend)
            <div class="first-heading">
              <h2 id="main-heading"> 
              {{$friend->fullname}}'s Profile Page
             
               <img class="img-circle" src ="{{ asset($friend->image->url('thumb'))}}" alt='photo'>
                 
            </h2>
           
            </div>
      
            <!--<div class='col-sm-12'>-->
                @if (Auth::check())
                <div class = "  newsfeed">
                
                  @if($friendship == 1)
                    {{ link_to_route('friend.unFriend', "UnFriend",array($friend->id,Auth::user() -> id))}}
                    <div></div>
                    {{ link_to_route('friend.newsFeed',"Friend's NewsFeed",array($friend->id ) ) }}
                     <div></div>
                    {{ link_to_route('friend.show',"Friend's friendlist",array($friend->id ) ) }}
                    @else
                    {{ link_to_route('friend.addFriend', "add Friend",array($friend->id,Auth::user() -> id))}}
                    
                   @endif
                </div>
                <div class="col-sm-3">
                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <span class="visible-xs navbar-brand">Sidebar menu</span>
                                </div>
                                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                  <ul class="nav navbar-nav">
                                    <!--<li class="active">{{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}</li>-->
                                    <li>{{ link_to_route('user.newsFeed',"My News Feed",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('friend.show',"My Friends List",array(Auth::user() -> id ) ) }}</li>
                                     
                                    <!--<li><a href="#">Reviews <span class="badge">1,118</span></a></li>-->
                                  </ul>
                                </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
            @endif
                <p></p>
                <p></p>
            <div class='col-sm-6'>
                
                    @if($friend) 
                    <div class = "profile">
                  
                   
                     <ul><li> Name : {{{$friend->fullname}}} </li></ul>
                    <ul><li> Date of Birth : {{{$friend->dob}}} </li></ul>
                     </div>
                    @endif 
                @else
                    <p> You Need to Login First !! </p>
                    <!--@if($users) -->
                    <!--    @foreach($users as $user)-->
                    <!--       <ul><li>{{ link_to_route('user.show', $user->fullname,array($user->id))}}</li></ul>-->
                    <!--    @endforeach-->
                    <!--@endif  -->
                
                @endif 
                
            </div> 
            
      </div>
  
@stop

