@extends ('master')

@section('title')
    Profile
@stop

@section('content')
   
    <div class='row'>
        <h3 class='well'>{{$user->fullname}}'s Profile</h3>
         <div class='col-sm-3'>
              <img class="img-circle" src ="{{ asset($user->image->url('thumb'))}}" alt='photo'>
              <div class = "profile"> 
                   Name: {{{$user->fullname}}} 
               <div></div>
                   age :{{{$age}}}
                  
              </div>      
        </div> 
    
    <div class='col-sm-9'>
        <h3 class='well'>Public Posts</h3>
                 @if($posts) 
                    @foreach($posts as $post)
                       <div class="panel">
                            <div class="panel-heading">
                                
                                {{{ $post->author}}} 
                                <div class ="text-center"> Title: {{{ $post->title}}}
                                     
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class=post-body>

                                  Message: {{{ $post->message}}}
                                 </div>
                                  
                            </div>
                            <div class="panel-footer">
                                 <ul class="list-inline">
                                     <li>{{ link_to_route('comment.show',"Comments",array($post->id)) }} </li>
                                  </ul>
                            </div>
                        </div>
                    @endforeach
                @endif  
        </div>
     </div> 
  @stop                
                   
                    
                   
                
                
          
            
      
  


