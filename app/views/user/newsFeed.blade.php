@extends ('master')

@section('title')
    User's News Feed
@stop

@section('content')
   
    <div class='row'>
            <div class="first-heading">
              <!--<h2 id="main-heading"></h2>-->
            </div>
           
               
            @if (Auth::check())
                <h3 class='well'>{{ Auth::user() -> fullname}}'s News Feed
                </h3>
                <div class="col-sm-3">
                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <span class="visible-xs navbar-brand">Sidebar menu</span>
                                </div>
                                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                  <ul class="nav navbar-nav">
                                    <!--<li class="active">{{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}</li>-->
                                    <li>{{ link_to_route('user.newsFeed',"My News Feed",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('friend.show',"My Friends List",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('friend.checkFriendship',"My Profile Page",array(Auth::user() -> id,Auth::user() -> id))}}</li>
                                  </ul>
                                </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
                    
                        <!--<h3 class='well'>{{ Auth::user() -> fullname}}'s News Feed</h3>-->
                        <!--<h3 class='well'> Posts</h3>-->
                 <div class='col-sm-6'>
                    <div class = "addPost">
                 
                    <!--￼{{ Form::open(array('url' => secure_url('user/addNew'),'class'=>'form')) }}-->
                    {{ Form :: open(array('method' => 'GET' , 'route' => array('post.addNew',Auth::user() -> id))) }}
                    {{ Form::label('title', 'title: ') }}
                    {{ Form::text('title',null,['class' => "form-control"]) }}
                     {{ $errors->first('title') }}
                       <p></p>
                    {{ Form::label('message', 'Message: ') }} 
                    {{ Form::text('message',null,['class' => "form-control " ]) }}
                    {{ $errors->first('message') }}
                    <p></p>
                    {{Form::label('privacy', 'Public')}}
                    {{ Form::radio('privacy', 'public',true) }}
                    {{Form::label('privacy', 'Friends')}}
                    {{ Form::radio('privacy', 'friends') }}
                    {{Form::label('privacy', 'private')}}
                    {{ Form::radio('privacy', 'private') }}
                    <p></p>
                    {{Form::submit('Create', ['class' => 'btn btn-large btn-primary openbutton'])}}
                    {{ Form::close() }}
                     </div>
                    <p></p>
                    <p></p>
                    <p></p>
                    @if($posts) 
                    @foreach($posts as $post)
                       <div class="panel">
                            <div class="panel-heading">
                                {{{ $post->author}}}
                                <div class ="text-center"> Title: {{{ $post->title }}}
                                    @if($post->user_id == Auth::user()->id) 
                                      <div class="dropdown">
                                          <button type="button" data-toggle="dropdown"><span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                             
                                              <li>{{ link_to_route('post.edit','Edit',array($post->id)) }}</li>
                                              
                                            
                                          </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class=post-body>

                                  Message: {{{ $post->message}}}
                                 </div>
                                  
                            </div>
                            <div class="panel-footer">
                                 <ul class="list-inline">
                                     <li>{{ link_to_route('comment.show',"Comments",array($post->id)) }} </li>
                                        @if($post->user_id == Auth::user()->id)    
                                            <div class = "delete">
                                               {{ Form :: open(array('method' => 'DELETE' , 'route' => array('post.destroy',$post->id))) }}
                                               {{ Form :: submit('Delete',array('class' => 'btn btn-danger'))  }}
                                               {{Form :: close() }}
                                             </div>
                                         @endif
                                  </ul>
                                 
                            </div>
                        </div>
                    @endforeach
                    @endif 
                     @if($posts) 
                    @foreach($postsFriends as $post)
                       <div class="panel">
                            <div class="panel-heading">
                                {{{ $post->author}}}
                                <div class ="text-center"> Title: {{{ $post->title }}}
                                    <!--@if($post->user_id == Auth::user()->id) -->
                                    <!--  <div class="dropdown">-->
                                    <!--      <button type="button" data-toggle="dropdown"><span class="caret"></span></button>-->
                                    <!--      <ul class="dropdown-menu">-->
                                             
                                    <!--          <li>{{ link_to_route('post.edit','Edit',array($post->id)) }}</li>-->
                                              
                                            
                                    <!--      </ul>-->
                                    <!--    </div>-->
                                    <!--@endif-->
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class=post-body>

                                  Message: {{{ $post->message}}}
                                 </div>
                                  
                            </div>
                            <div class="panel-footer">
                                 <ul class="list-inline">
                                     <li>{{ link_to_route('comment.show',"Comments",array($post->id)) }} </li>
                                        <!--@if($post->user_id == Auth::user()->id)    -->
                                        <!--    <div class = "delete">-->
                                        <!--       {{ Form :: open(array('method' => 'DELETE' , 'route' => array('post.destroy',$post->id))) }}-->
                                        <!--       {{ Form :: submit('Delete',array('class' => 'btn btn-danger'))  }}-->
                                        <!--       {{Form :: close() }}-->
                                        <!--     </div>-->
                                        <!-- @endif-->
                                  </ul>
                                 
                            </div>
                        </div>
                    @endforeach
                    @endif 
                   
                </div> 
                <div class="col-sm-3">
                    {{ Auth::user() -> fullname}} {{ link_to_route('user.logout',"Sign Out") }}
                </div>
            @endif    
      </div>
  
@stop

