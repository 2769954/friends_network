@extends ('master')

@section('title')
    showUsers
@stop

@section('content')
   
    <div class='row'>
           
            <div class="first-heading">
              <h2 id="main-heading">User Search Result</h2>
            </div>
            <!--<div class='col-sm-12'>-->
        @if (Auth::check())
            <div class = "  newsfeed">
                {{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}
            </div>
            <div class="col-sm-3">
                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <span class="visible-xs navbar-brand">Sidebar menu</span>
                                </div>
                                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                  <ul class="nav navbar-nav">
                                    <!--<li class="active">{{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}</li>-->
                                    <li>{{ link_to_route('user.newsFeed',"My News Feed",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('friend.show',"My Friends List",array(Auth::user() -> id ) ) }}</li>
                                    <!--<li><a href="#">Reviews <span class="badge">1,118</span></a></li>-->
                                  </ul>
                                </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
                <p></p>
                <p></p>
            <div class='col-sm-9'>
                      @if($users) 
                        @foreach($users as $user)
                       <div class="name">
                        <img class="img-circle" src ="{{ asset($user->image->url('thumb'))}}" alt='photo'>
                        {{ link_to_route('friend.checkFriendship', $user->fullname,array($user->id,Auth::user() -> id))}}
                       </div>
                        @endforeach
                    @endif 
         @else
                    @if($users) 
                        @foreach($users as $user)
                        <div class="name">
                        <img class="img-circle" src ="{{ asset($user->image->url('thumb'))}}" alt='photo'>
                        {{ link_to_route('user.show', $user->fullname,array($user->id))}}
                        </div>
                        @endforeach
                    @endif  
                
        @endif 
                
            </div>  
      </div>
  
@stop

