@extends ('master')

@section('title')
    Sign Up
@stop

@section('content')
   
    <div class='row'>
            <div class="first-heading">
              <h2 id="main-heading">New User Page</h2>
            </div>
            
         <div class='col-sm-12'>        
            @if (Auth::check())
                    {{ Auth::user() -> username }} {{ link_to_route('user.logout',"Sign Out") }} 
                    <h3>You are already signed in</h3>
            @else
                <!--<div class="form-group">-->
            
                <div >
                    
                    {{ Form::open(array('route' => 'user.store', 'files' => true)) }}
                    {{ Form::label('username', 'UserName: ') }}
                    {{ Form::text('username',null,['class' => "form-control", 'placeholder'=>'xyz@a.org']) }}
                     <!--{{ $errors->first('UserName', '<p class="help-block">:message</p>') }}-->
                    {{ $errors->first('username') }}
                    <p></p>
                    {{ Form::label('fullname', 'FullName:') }}
                    {{ Form::text('fullname',null,['class' => "form-control",'placeholder'=>'enetr your name']) }}
                    {{ $errors->first('fullname') }}
                    <p></p>
                    {{ Form::label('password', 'Password: ') }} 
                    {{ Form::password('password',['class' => "form-control"]) }}
                    <!--{{ $errors->first('password', '<p class="help-block">:message</p>') }}-->
                    {{ $errors->first('password') }}
                       <p></p>
                    <!--{{Session::get('login_error')}}-->
                    {{ Form::label('dateOfBirth', 'Date of Birth:') }}
                    <p></p>
                    {{ Form::text('year',null,['placeholder'=>'year']) }}
                    {{ $errors->first('year') }}
                    {{ Form::text('month',null,['placeholder'=>'month']) }}
                    {{ $errors->first('month') }}
                    {{ Form::text('day',null,['placeholder'=>'day']) }}
                    {{ $errors->first('day') }}
                    <p></p>
                    {{ Form::label('image', 'Image:') }}
                    {{ Form::file('image') }}
                     <p></p>
                    {{Form::submit('Sign UP', ['class' => 'btn btn-large btn-primary openbutton'])}}
                    {{ Form::close() }}
               
               </div>   
            @endif
               </div> 
      
      </div>
  
@stop   
                
          
  

