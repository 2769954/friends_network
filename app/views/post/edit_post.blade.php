@extends ('master')

@section('title')
    Edit Post
@stop

@section('content')
   
    <div class='row'>
            <div class="first-heading">
              <!--<h2 id="main-heading"></h2>-->
            </div>
           
               
            @if (Auth::check())
                
                <div class="col-sm-3">
                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <span class="visible-xs navbar-brand">Sidebar menu</span>
                                </div>
                                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                  <ul class="nav navbar-nav">
                                    <!--<li class="active">{{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}</li>-->
                                    <li>{{ link_to_route('user.newsFeed',"My News Feed",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('friend.show',"My Friends List",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('post.addNew',"Add a new Post",array(Auth::user() -> id ) ) }}</li>
                                    <!--<li><a href="#">Reviews <span class="badge">1,118</span></a></li>-->
                                  </ul>
                                </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
                 <div class='col-sm-6'>
                    <div class = "addPost">
                 
                         {{Form::model($post, array('method' => 'PUT', 
                            'route' =>array('post.update', $post->id))) }}
                            {{ Form::label('title', 'Post Title: ') }}
                            {{ Form::text('title') }}
                            {{ $errors->first('title') }}
                            <p></p> 
                            
                            {{ Form::label('message', 'message: ') }} 
                            {{ Form::text('message') }}
                            {{ $errors->first('message') }}
                            <p></p> 
                            {{Form::label('privacy', 'Public')}}
                            {{ Form::radio('privacy', 'public') }}
                            {{Form::label('privacy', 'Friends')}}
                            {{ Form::radio('privacy', 'friends') }}
                            {{Form::label('privacy', 'private')}}
                            {{ Form::radio('privacy', 'private') }}
                             <p></p>
                            {{ Form::submit('Update') }} 
                             
                        {{ Form::close() }}   

                    </div>
        
                </div> 
                <div class="col-sm-3">
                    {{ Auth::user() -> fullname}} {{ link_to_route('user.logout',"Sign Out") }}
                </div>
            @else
              <p>You need to logged in!!</p>
            @endif    
      </div>
  
@stop

