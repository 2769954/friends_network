@extends ('master')

@section('title')
    Home
@stop

@section('content')
   
    <div class='row'>
           <div class="searchBox">
             <!--￼{{ Form::open(array('action' => 'UserController@search')) }}-->
             {{ Form :: open(array('method' => 'GET' , 'route' => array('user.search'))) }}
                    {{ Form::label('username', 'UserName: ') }}
                    {{ Form::text('username',null,[ 'placeholder'=>'Search a User']) }}
                    {{Form::submit('Search', ['class' => ' btn-primary '])}}
            {{ Form::close() }}
            </div>
            <div class="first-heading">
              <h2 id="main-heading">Home Page</h2>
            </div>
           
         <div class='col-sm-12'>       
            @if (Auth::check())
                {{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }} 
                <div class = "newsfeed">
                {{ link_to_route('user.newsFeed',"My News Feed",array(Auth::user() -> id ) ) }}
                </div>
                <div></div>
             
                <h3 class='well'>Public Posts</h3>
                 @if($posts) 
                    @foreach($posts as $post)
                       <div class="panel">
                            <div class="panel-heading">
                                
                                {{{ $post->author}}} 
                                <div class ="text-center"> Title: {{{ $post->title}}}
                                     
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class=post-body>

                                  Message: {{{ $post->message}}}
                                 </div>
                                  
                            </div>
                            <div class="panel-footer">
                                 <ul class="list-inline">
                                     <li>{{ link_to_route('comment.show',"Comments",array($post->id)) }} </li>
                                  </ul>
                            </div>
                        </div>
                    @endforeach
                @endif  
            @else
                <div class="form-group">
                    ￼{{ Form::open(array('url' => secure_url('user/login'),'class'=>'form')) }}
                    {{ Form::label('username', 'UserName: ') }}
                    {{ Form::text('username',null,['class' => "form-control"]) }}
                     {{Session::get('login_error')}}
                      <p></p>
                    {{ Form::label('password', 'Password: ') }} 
                    {{ Form::password('password',['class' => "form-control"]) }}
                     {{ $errors->first('price') }}
                    <p></p>
                     
                    {{Form::submit('Sign in', ['class' => 'btn btn-large btn-primary openbutton'])}}
                    {{ Form::close() }}
                 </div> 
                <h3 class='well'>Public Posts</h3>
                @if($posts) 
                    @foreach($posts as $post)
                       <div class="panel">
                            <div class="panel-heading">
                                {{{ $post->author}}} 
                                <div class ="text-center"> Title: {{{ $post->title}}}
                                     
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class=post-body>

                                  Message: {{{ $post->message}}}
                                 </div>
                                  
                            </div>
                            <div class="panel-footer">
                                 <ul class="list-inline">
                                  <!--{{-- <li> <a href="{{{ url("comments_page/$item->id") }}}">Comments <span class="badge">{{{$item->msg_count}}}</span></a></li>--}}-->
                                   <!--<li> <a href="#">likes <span class="badge">10</span></a></li>-->
                                  </ul>
                            </div>
                        </div>
                    @endforeach
                @endif  
                
            @endif 
                
            

            </div>  
      </div>
  
@stop

