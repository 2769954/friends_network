@extends('master')

@section('title')
ERD Diagram
@stop

@section('content')

    <h2>ERD Diagram</h2>
    {{ HTML::image('erd3.png','ERD Photo',array('class' => 'erd_img')) }}

@stop