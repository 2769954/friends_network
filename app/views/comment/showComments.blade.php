@extends ('master')

@section('title')
    Comments Page
@stop

@section('content')
   
    <div class='row'>
            <div class="first-heading">
              <!--<h2 id="main-heading"></h2>-->
            </div>
           
               
            @if (Auth::check())
                <!--<h3 class='well'>{{ Auth::user() -> fullname}}'s News Feed</h3>-->
                <div class="col-sm-3">
                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <span class="visible-xs navbar-brand">Sidebar menu</span>
                                </div>
                                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                  <ul class="nav navbar-nav">
                                    <!--<li class="active">{{ Auth::user() -> fullname }} {{ link_to_route('user.logout',"Sign Out") }}</li>-->
                                    <li>{{ link_to_route('user.newsFeed',"My News Feed",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('friend.show',"My Friends List",array(Auth::user() -> id ) ) }}</li>
                                    <li>{{ link_to_route('post.addNew',"Add a new Post",array(Auth::user() -> id ) ) }}</li>
                                    <!--<li><a href="#">Reviews <span class="badge">1,118</span></a></li>-->
                                  </ul>
                                </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
                    
                        <!--<h3 class='well'>{{ Auth::user() -> fullname}}'s News Feed</h3>-->
                        <!--<h3 class='well'> Posts</h3>-->
                <div class='col-sm-6'>
                @if($post)
                    <div class="panel">
                            <div class="panel-heading">
                                {{{ $post->author}}}
                                <div class ="text-center"> 
                                Title: {{ $post->title }}
                                
                                      <div class="dropdown">
                                          <button type="button" data-toggle="dropdown"><span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                               {{--<li>{{{ link_to_route('post.show',array($post->id)) }}} Delete </li> --}}
                                              {{-- <li>{{{ link_to_route('post.edit', $post->id,array($post->id)) }}} </li> --}}
                                    
                                          </ul>
                                        </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class=post-body>
                                     {{{ $post->message}}} 
                                </div>
                                   
                            </div>
                            <div class="panel-footer">
                                 <ul class="list-inline">
                                     <li>{{ link_to_route('comment.show',"Comments",array($post->id)) }} </li>
                    
                                  </ul>
                            </div>
                            
                        </div> 
                        
                    <div class = "addPost">
                 
                    <!--￼{{ Form::open(array('url' => secure_url('user/addNew'),'class'=>'form')) }}-->
                    {{ Form :: open(array('method' => 'GET' , 'route' => array('comment.addNew',$post -> id,Auth::user() -> id))) }}
                       <p></p>
                    {{ Form::label('message', 'Message: ') }} 
                    {{ Form::text('message',null,['class' => "form-control " ]) }}
                     {{ $errors->first('message') }}
                    <p></p>
    
                    {{Form::submit('Create', ['class' => 'btn btn-large btn-primary openbutton'])}}
                    {{ Form::close() }}
                     </div>
                    <p></p>
                    <p></p>
                        @if($comments) 
                        @foreach($comments as $comment)
                          
                            <div class="panel">
                                   <div class="panel-heading">
                                       
                                        <div class ="text-center"> {{{ $comment->author }}}
                                         @if($comment->user_id == Auth::user()->id) 
                                             <div class = "delete">
                                               {{ Form :: open(array('method' => 'DELETE' , 'route' => array('comment.destroy',$comment->id))) }}
                                               {{ Form :: submit('Delete')  }}
                                               {{Form :: close() }}
                                             </div>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                    <div class=post-body>
                                       {{{ $comment->message}}}
                                     </div>
                                    </div>
                                    <div class="panel-footer"> 
                                        
                                    </div>
                            </div>
                        @endforeach
                        @endif 
                          {{$comments->links()}}
                @endif
                   
            </div> 
            <div class="col-sm-3">
                    {{ Auth::user() -> fullname}} {{ link_to_route('user.logout',"Sign Out") }}
                </div>
            @endif    
      </div>
  
@stop

