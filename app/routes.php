<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// route to login
Route::post('user/login',array('as' => 'user.login', 'uses' => 'UserController@login'));

//route to logiut
Route::get('user/logout',array('as' => 'user.logout', 'uses' => 'UserController@logout'));

// route to show the newsfeed of a particular user
Route::get('user/{id}/newsFeed',array('as' => 'user.newsFeed', 'uses' => 'UserController@newsFeed'));

//route to search all the existing users 
Route::get('user/search',array('as' => 'user.search', 'uses' => 'UserController@search'));

//route to show the profile page of a friend
Route::get('friend/{id}/profile',array('as' => 'friend.profile', 'uses' => 'FriendController@profile'));

//route to show the newsFeed of a friend
Route::get('friend/{id}/newsFeed',array('as' => 'friend.newsFeed', 'uses' => 'FriendController@newsFeed'));

//route to create a new post
Route::get('post/{id}/addNew',array('as' => 'post.addNew', 'uses' => 'PostController@addNew'));

//route to create a new comment
Route::get('comment/{id}/{id2}/addNew',array('as' => 'comment.addNew', 'uses' => 'CommentController@addNew'));

//route to check friendship between two users
Route::get('friend/{id}/{id2}/profile',array('as' => 'friend.checkFriendship', 'uses' => 'FriendController@checkFriendship'));

//route to add friend
Route::get('friend/{id}/{id2}/addFriend',array('as' => 'friend.addFriend', 'uses' => 'FriendController@addFriend'));

//route to un-friend a friend
Route::get('friend/{id}/{id2}/unFriend',array('as' => 'friend.unFriend', 'uses' => 'FriendController@unFriend'));


//create Controller for users table
Route::resource('user','UserController');

//create Controller for friends table
Route::resource('friend','FriendController');

//create Controller for posts table
Route::resource('post','PostController');

//create controller for comments table
Route::resource('comment','CommentController');

//Home page
Route::get('/',array('as' => 'home', 'uses' => 'PostController@index'));
// Route::get('/', function()
// {
// 	return View::make('hello');
// });
