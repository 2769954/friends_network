<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	// use UserTrait, RemindableTrait;
	use EloquentTrait, UserTrait, RemindableTrait;


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	public function __construct(array $attributes = array()) {
			$this->hasAttachedFile('image', [
			'styles' => [
			'medium' => '300x300',
			'thumb' => '100x100'
			]
	]);
			parent::__construct($attributes);
	}
	
	public static $rules = array(
        'username' => 'required|email|unique:users',
        'password' => 'required|min:4',
        'year'     => 'required|min:4',
        'month'    => 'required',
        'day'      => 'required',
         'fullname'    => 'required',
        );

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	function posts()
	{
		return $this->hasMany('Post');
	}
		function comments()
	{
		return $this->hasMany('Comment');
	}
		function friends()
	{
		return $this->belongsToMany('User','friends','user_id','friend_id');
	}
	
	function getDates()
	{
		return array('created_at',
		'updated_at', 'birth_at');
	}
}
