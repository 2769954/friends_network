<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		 
	    $posts = Post::whereRaw('privacy = "public" ')->orderBy('id', 'DESC')->get();
		return View::make('post.index', compact('posts'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('post.erd_diagram');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		print_r("in show");
		// $comments = Post::find($id)->comments;
		// return View::make('products.show', compact('product'));
	}


	/**
	 * Edit a post
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	    $post = Post::find($id);
		return View::make('post.edit_post', compact('post'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	
	    $post = Post::find($id);
		$input = Input::all();
		$v = Validator :: make($input, Post::$rules);
		if($v -> passes()){
			$post->title = $input['title']; 
			$post->message = $input['message']; 
			$post->privacy = $input['privacy'];
			User::find($post->user_id)->posts()->save($post);
			return Redirect::action('UserController@newsFeed',$post->user_id);
		
		}else{
		 	return Redirect::action('PostController@edit')->withErrors($v);
			
		 }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 $post = Post::find($id);
		 $user_id = $post->user_id;
		 $post->delete();
		 return Redirect::action('UserController@newsFeed',$user_id);
	}


	/**
	 * add a new post.
	 *
	 * @param  int  $id of the user who creates this post
	 * @return Response
	 */
	public function addNew($id)
	{
		$input = Input::all();
		$v = Validator :: make($input, Post::$rules);
		if($v -> passes()){
		
		    $post = new Post(); 
			$post->title = $input['title']; 
			$post->message = $input['message']; 
			$post->privacy = $input['privacy'];
			$user = User::find($id);
			$post->author = $user->fullname;
			User::find($id)->posts()->save($post);
			return Redirect::action('UserController@newsFeed',$id);
		
		}else{
		 	return Redirect::action('UserController@newsFeed',$id)->withErrors($v);
			
		 }
	}
}
