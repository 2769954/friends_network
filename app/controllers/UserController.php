<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('user.report');	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{  
		$posts = Post::whereRaw('privacy = "public" ')->get();
        return View::make('user.create_user', compact('posts'));
	  
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	
		$input = Input::all();
		$v = Validator :: make($input, User::$rules);
		if($v -> passes()){
			$password = $input['password'];
			$encrypted = Hash::make($password);
		    $user = new User(); 
			$user->username = $input['username']; 
			$user->password = $encrypted; 
			$y=($input['year']);
		    $m=($input['month']);
		    $d=($input['day']);
		    $dob = "$y"."-$m-"."$d";
			$user->dob = $dob; 
			$user->fullname = $input['fullname'];
			$user->image = $input['image'];
			$user->save();
			return Redirect::action('PostController@index');
		
		}else{
		 	return Redirect::action('UserController@create')->withErrors($v);
			
		 }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	   $user = User::find($id);
	   $posts = Post::whereRaw('privacy = "public" ')->get();
       $d = Carbon\Carbon::createFromFormat('Y-m-d', $user->dob)->toDateString();
       $age = Carbon\Carbon::createFromFormat('Y-m-d', $user->dob)->age;
       return View::make('user.profile_not_logged_in', compact('user','age','posts'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function login()
	{
	
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
			);
	   
		if (Auth::attempt($userdata))
		{
			$val = $userdata["username"];
			$user = User::whereRaw('username = ? ', array($val))->get();
			//$user = User::where(' username' , '=', $val)->get();
			$id = $user[0]->id;
			$posts = User::find($user[0]->id)->posts;
		    return Redirect::action('UserController@newsFeed',$id);
		}else{
			// Session::put('login_error','Invalid username or password');
			Session::flash('login_error','Invalid username or password');
			// $value = Session::get('login_error');
			return Redirect::to(URL::previous())->withInput();
		}
		
	}

	
	public function logout(){
		Auth::logout();
		return Redirect::action('PostController@index');
	}
	
	// shows the newsfeed of a user with authorised posts
	public function newsFeed($id){
		

		 $sql= "  select * from posts p ,users u,friends f  where u.id = f.user_id AND  u.id=? And f.friend_id=p.user_id ANd 
					 p.privacy = 'friends'";
        $postsFriends = DB::select($sql, array($id));
         //print_r($posts[0]->title);
		$ownPosts = User::find($id)->posts()->whereRaw('privacy != "public"')->orderBy('id', 'DESC')->get();
		$publicPosts = Post::whereRaw('privacy = "public" ')->orderBy('id', 'DESC')->get();
		$posts = $ownPosts->merge($publicPosts);
		// $posts = $posts1->merge($postsFriends);
        return View::make('user.newsFeed', compact('posts','postsFriends'));
	}
	
	//searh a user
	public function search(){
		// $input = Input::get('username');
	    $input = Input::all();
	    $val = $input['username'];
		$users = User::whereRaw('fullname like ? ', array("%$val%"))->get();
		return View::make('user.showUsers', compact('users'));
	}
}
