<?php

class FriendController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Show a list of all friends.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$friends = User::find($id)->friends;
		return View::make('friend.showFriends', compact('friends'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

// show profile page of a friend
    public function profile($id){
    	$friend = User::find($id);
    	echo $birth = Carbon\Carbon::createFromFormat('Y-m-d', $friend->dob)->toDateString();
        return View::make('friend.profile', compact('friend','birth'));
    }
    
// show newsFeed of a friend
    public function newsFeed($id){
    	$ownPosts = User::find($id)->posts()->whereRaw('privacy != ?', array("private"))->orderBy('id', 'DESC')->get();;
    	$publicPosts = Post::whereRaw('privacy = ?', array("public"))->orderBy('id', 'DESC')->get();;
    	$posts = $ownPosts->merge($publicPosts);
    	$friend = User::find($id);
		return View::make('friend.newsFeed', compact('posts','friend'));
    }
    
  	/**
	 * Check the friendship of two users.
	 *
	 * @param  int  $id of user2 and $user_id of user1
	 * @return Response
	 */
    public function checkFriendship($friend_id,$user_id){
    	// print_r("$friend_id, $user_id");
    	$friends = User::find($user_id)->friends()->whereRaw('friend_id = ?', array($friend_id))->get();
    	$friendship = 0;
    	if($friend_id != $user_id){  //if visiting own profile
	    	if(count($friends)!= 0){
	    		$friendship = 1;
	    		$friend = User::find($friends[0]->id);
	    		return View::make('friend.profile', compact('friend','friendship'));
	    		
	    	}else{
	    		$friend = User::find($friend_id);
	    		return View::make('friend.profile', compact('friend','friendship'));
	    	}
    	}else{
    		$friend = User::find($user_id);
    		return View::make('friend.own_profile', compact('friend'));
    	}
    }
    /**
	 * add a friend.
	 *
	 * @param  int  $id of user2 and $user_id of user1
	 * @return Response
	 */
    public function addFriend($friend_id,$user_id){
        $mytime = Carbon\Carbon::now();
    	User::find($user_id)->friends()->attach($friend_id, array("updated_at"=> $mytime->toDateTimeString(),"created_at"=> $mytime->toDateTimeString() ));
    	$friend = User::find($friend_id);
    	$friend->friends()->attach($user_id,array("updated_at"=> $mytime->toDateTimeString(),"created_at"=> $mytime->toDateTimeString()));
    	$friends = User::find($user_id)->friends;
		return View::make('friend.showFriends', compact('friends'));
    }
    
    /**
	 * unfriend a friend.
	 *
	 * @param  int  $id of user2(friend) and $user_id of user1(logeed in user)
	 * @return Response
	 */
    public function unFriend($friend_id,$user_id){
        
    	User::find($user_id)->friends()->detach($friend_id);
    	$friend = User::find($friend_id);
    	$friend->friends()->detach($user_id);
    	$friends = User::find($user_id)->friends;
		return View::make('friend.showFriends', compact('friends'));
    	
    }
}
