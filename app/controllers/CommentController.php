<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{   
		$post = Post::find($id);
	    $comments = Post::find($id)->comments()->paginate(6);
	    return View::make('comment.showComments', compact('comments','post'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 $comment = Comment::find($id);
		 $post_id = $comment->post_id;
		 $comment->delete();
		 return Redirect::action('CommentController@show',$post_id);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function addNew($id,$user_id)
	{
		$input = Input::all();
		$name = User::find($user_id);
		print_r($name->fullname);
		$v = Validator :: make($input, Comment::$rules);
		if($v -> passes()){
		
		    $comment = new Comment(); 
			$comment->message = $input['message'];
			$comment->author = $name->fullname;
			$comment->user_id = $user_id;
			Post::find($id)->comments()->save($comment);
			return Redirect::action('CommentController@show',$id);
		
		}else{
		 	return Redirect::action('CommentController@show',$id)->withErrors($v);
			
		 }
	}

}
