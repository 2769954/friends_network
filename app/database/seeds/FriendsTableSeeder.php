<?php


class FriendsTableSeeder extends Seeder {
public function run()
    {
        $friend = new Friend;
        $friend->user_id = 1;
        $friend->friend_id = 2;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 2;
        $friend->friend_id = 1;
        $friend->save();
        
        
        $friend = new Friend;
        $friend->user_id = 1;
        $friend->friend_id = 4;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 4;
        $friend->friend_id = 1;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 1;
        $friend->friend_id = 5;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 5;
        $friend->friend_id = 1;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 9;
        $friend->friend_id = 10;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 10;
        $friend->friend_id = 9;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 9;
        $friend->friend_id = 8;
        $friend->save();
        
        $friend = new Friend;
        $friend->user_id = 8;
        $friend->friend_id = 9;
        $friend->save();
        
        
    }
}