<?php


class PostsTableSeeder extends Seeder {
public function run()
    {
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 1";
        $post->author = "john";
        $post->message = "public message 1";
        $post->privacy = 'public' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 2";
        $post->message = "public message 2";
         $post->author = "john";
        $post->privacy = 'public' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 3";
        $post->message = "public message 3";
         $post->author = "john";
        $post->privacy = 'public' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 4";
        $post->message = "public message 4";
         $post->author = "john";
        $post->privacy = 'public' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 5";
        $post->author = "john";
        $post->message = " public message 5";
        $post->privacy = 'public' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 6";
        $post->message = "private message 6";
        $post->author = "john";
        $post->privacy = 'private' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 7";
        $post->message = "private message 7";
        $post->author = "john";
        $post->privacy = 'private' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 8";
        $post->message = "friends message 8";
        $post->author = "john";
        $post->privacy = 'friends' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 9";
        $post->message = "friends message 9";
        $post->author = "john";
        $post->privacy = 'friends' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 2;
        $post->title = "john's post 10";
        $post->author = "john";
        $post->message = "friends message 10";
        $post->privacy = 'friends' ;
        $post->save();
        
         
        $post = new Post;
        $post->user_id = 1;
        $post->title = "bob's post 1";
        $post->message = "bobs public post";
        $post->author = "bob";
        $post->privacy = 'public' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 1;
        $post->title = "bob's post 2";
        $post->message = "bobs friends post";
        $post->author = "bob";
        $post->privacy = 'friends' ;
        $post->save();
        
        $post = new Post;
        $post->user_id = 1;
        $post->title = "bob's post 3";
        $post->author = "bob";
        $post->message = "bobs private post";
        $post->privacy = 'private' ;
        $post->save();
    }
}