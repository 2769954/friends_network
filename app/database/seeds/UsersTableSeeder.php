<?php


class UsersTableSeeder extends Seeder {
public function run()
    {
        $user = new User;
        $user->username = 'bob@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'bob';
        $user->dob = '2014-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'john@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'john';
        $user->dob = '1985-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'tom@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'tom';
        $user->dob = '1965-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'alix@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'alix';
        $user->dob = '1975-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'harris@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'haris';
         $user->dob = '1945-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'rob@a.org';
         $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'rob';
         $user->dob = '1955-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'Gym@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'Gym';
         $user->dob = '1975-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'holder@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'holder';
         $user->dob = '1915-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'ralph@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'ralph';
        $user->dob = '1985-05-06';
        $user->save();
        
        $user = new User;
        $user->username = 'michael@a.org';
        $password = '1234';
        $encrypted = Hash::make($password);
        $user->password = $encrypted;
        $user->fullname = 'michael';
        $user->dob = '1985-05-06';
        $user->save();
    }
}